package pvdv.ul.ie.labweek2_activities;

        import android.app.Activity;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.widget.Button;
        import android.widget.EditText;

public class AddActivity extends Activity {

    Button storeButton;
    EditText itemForm;

    Button newButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_screen);

        storeButton = (Button) findViewById(R.id.storebutton);

        itemForm = (EditText) findViewById(R.id.itemForm);

        newButton = (Button) findViewById(R.id.newButton);


        if (MainActivity.storedItems.size()>0) {
            itemForm.setText(MainActivity.storedItems.get(0));
        }

        storeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemToList();
            }
        });

        newButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                itemForm.setText("");
            }
        });

    }






    private void addItemToList() {
        MainActivity.storedItems.add(itemForm.getText().toString());
        itemForm.setText("");
    }


}

