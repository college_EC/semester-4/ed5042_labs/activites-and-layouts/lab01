package pvdv.ul.ie.labweek2_activities;

/* Student name: Eoghan Conlon
 * Student id: 21310262
 * Partner name: N/A
 * Partner id: N/A
 */

        import java.util.ArrayList;
        import android.os.Bundle;
        import android.app.Activity;
        import android.content.Intent;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.widget.Button;
        import android.widget.TextView;

public class MainActivity extends Activity {

    Button addButton;
    Button viewButton;
    TextView instructionText;

    /*
     * Note: the array list is made static so that it can be referenced in other activities. This is NOT the preferred way of sharing information between
     * activities, but is done here to limit the introduction of new features. Once you understand the life cycle of activities, you will understand this is actually a very dangerous idea :-)
     */
    public static ArrayList<String> storedItems = new ArrayList<String>();

    /*
     * onCreate() sets up the layout file, instantiates and initialises the various buttons and sets up listeners on these buttons
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // note the 'super.onCreate' statement: we re-use the functionality provided by the super class for onCreate()
        super.onCreate(savedInstanceState);
        // set this activity's GUI to activity_main (see folder: res->layout)
        setContentView(R.layout.activity_main);
        instructionText = (TextView) findViewById(R.id.instruction_text);

        // so far the viewButton field and TextView field have not been initialised (only declared). Here they are given references to the elements in the layout:
        addButton = (Button) findViewById(R.id.addbutton);

        // set the text shown to the user when the activity is started first:
        instructionText.setText(R.string.initial_instruction);

        viewButton = (Button) findViewById(R.id.viewbutton);

        // the OnClickListener reacts to a button click and runs onClick()
        addButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
				/*
				 * The generic way to start a new activity:
				 * 		1. Instantiate a new Intent with the context of this activity and the Activity class that should be started
				 * 		2. start the activity with startActivity()
				 */
                Intent addActivityIntent = new Intent(getBaseContext(), AddActivity.class);
                startActivity(addActivityIntent);

            }

        });

        viewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Code refactored from lines 57 & 58
                Intent viewButtonIntent = new Intent(getBaseContext(), ViewActivity.class);
                startActivity(viewButtonIntent);
            }

        });


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        instructionText.setText(R.string.return_instruction);
    }



}
