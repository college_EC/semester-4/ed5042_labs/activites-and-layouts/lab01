package pvdv.ul.ie.labweek2_activities;

/**
 * Created by awal on 01/02/2016.
 */

/*
 * The ListActivity is a special activity used to display a list of items. The object that is responsible for populating the list with the data is called ListAdapter.
 *  You do not need to provide a layout. You need to:
 * 	1. provide the list to be displayed - MainActivity.storedItems in this case
 *  2. the layout file for one of the elements of the list
 */

        import android.app.ListActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.AdapterView.OnItemClickListener;
        import android.widget.ArrayAdapter;
        import android.widget.ListView;

public class ViewActivity extends ListActivity {

    ListView itemList;
    ArrayAdapter adapter;

	/*
	 * Just like for a normal activity, the various GUI related objects are initialised in the onCreate(). In this case a list adapter is setup
	 * and an event listener is attached to the listview.
	 */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // instantiate a new ArrayAdapter which populates the list with the strings found in MainActivity.storedItems.
        adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.list_item, MainActivity.storedItems);
        // the layout used for each list item is taken from R.layout.list_item (see folder: res->layout)
        setListAdapter(adapter);
        // get a reference to the listview
        ListView lv = getListView();



        // we want to react to the user clicking on an item in the listview. For this reason we implement an onItemClickListener
        // The onItemClickListener will execute the onItemClick method whenever an item is chosen.
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                removeItem(position);

            }  });
    }

    /*
     * It is not strictly necessary to move the functionality of the onItemClick method out to this method, but it can be useful for clarity
     * (especially if many onItemClick listeners are instantiated in onCreate(). Potential advantage is that the functionality of the onItemClick
     *  method can be quickly changed and/or changed back to the original function simply be creating a second method and calling this method.
     */

    private void removeItem(int position) {
        MainActivity.storedItems.remove(position);
        // send notification that the data set has changed. This is necessary for the listview to update correctly
        adapter.notifyDataSetChanged();
    }
}

